#include <iostream>
#include <time.h>

int main() {
    int jeu[3], y = 3;
    bool hasFour = false, hasTwo = false, hasOne = false;
    srand(time(nullptr));
    for (int i = 0; i < 3; ++i) {
        std::cout << "Voici votre jet :" << std::endl;
        for (int j = 0; j < y; ++j) {
            jeu[j] = rand() % 6 + 1;
        }
        std::cout << jeu[0] << " " << jeu[1] << " " << jeu[2] << std::endl;
        if ((jeu[0] == 4 || jeu[1] == 4 || jeu[2] == 4) && !hasFour) {
            hasFour = true;
            y = y - 1;
        }
        if ((jeu[0] == 2 || jeu[1] == 2 || jeu[2] == 2) && !hasTwo) {
            hasTwo = true;
            y = y - 1;
        }
        if ((jeu[0] == 1 || jeu[1] == 1 || jeu[2] == 1) && !hasOne) {
            hasOne = true;
            y = y - 1;
        }
        if (y == 0) { break; }
    }
    std::cout << (y == 0 ? "bravo !! gagné" : "Dommage, c'est perdu") << std::endl;
    return 0;
}