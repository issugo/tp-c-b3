#include <iostream>
#include <math.h>

int main(int argc, char* argv[]) {
    std::cout << "Exponentielle de " << argv[1] << " = " << exp(atoi(argv[1])) << std::endl;
    std::cout << "Cosinus de " << argv[1] << " = " << cos(atoi(argv[1])) << std::endl;
    std::cout << "Sinus de " << argv[1] << " = " << sin(atoi(argv[1])) << std::endl;
    return 0;
}