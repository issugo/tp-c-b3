#include <iostream>
#include <time.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
    char retry = 'N';
    do {
        srand(time(nullptr));
        int number = atoi(argv[1]), random = rand() % (number * 2) - number, coup = 0, guess;
        std::cout << "Le nombre à trouver est compris entre " << (number * -1) << " et " << number << std::endl;
        do {
            std::cout << "Entrez un nombre : ", std::cin >> guess;
            std::cout << (guess > random ? "Trop grand" : (guess < random ? "Trop petit" : "")) << std::endl;
            ++coup;
        } while (guess != random);
        std::cout << "Bravo le chiffre est bien " << random << ", vous avez gagné en " << coup << " coup(s)." << std::endl;
        std::cout << "Voulez-vous recommencez : ", std::cin >> retry;
    }while (retry == 'o' || retry == 'O');
    return 0;
}