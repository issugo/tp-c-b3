cmake_minimum_required(VERSION 3.17)
project(tp1_1)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_EXE_LINKER_FLAGS "-static")

add_executable(tp1_1 main.cpp)