cmake_minimum_required(VERSION 3.17)
project(tp1_8)

set(CMAKE_CXX_STANDARD 11)

add_executable(tp1_8 main.cpp)