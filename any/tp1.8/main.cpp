#include <iostream>

int main(int argc, char* argv[]) {
    int result = 1, temp = 0;
    while (result < atoi(argv[1])) {
        ++temp;
        result = result * temp;
    }
    std::cout << temp-1 << "! <= " << argv[1] << std::endl;
    return 0;
}