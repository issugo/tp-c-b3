#include <iostream>
#include <time.h>

int main(int argc, char* argv[]) {
    srand(time(NULL));
    int nbAlumettes = atoi(argv[1]), enlevage = 0, joueur = 1;
    joueur = rand() % 2 + 1;
    std::cout << "nombre d'alumettes de départ : " << nbAlumettes << std::endl;
    do {
        joueur == 1 ? joueur = 2 : joueur = 1;
        std::cout << "joueur " << joueur << " enlève : ", std::cin >> enlevage;
        nbAlumettes = nbAlumettes - enlevage;
        std::cout << "Il reste " << nbAlumettes << " d'alumettes" << std::endl;
    }while (nbAlumettes > 0);
    std::cout << "joueur " << joueur << " a perdu !!" << std::endl;
    return 0;
}