#include <iostream>

int main(int argc, char* argv[]) {
    int debut = atoi(argv[1]), n = atoi(argv[1]), u0 = 0, u1 = 1, result;
    for (int i = 0; i < n-1; ++i) {
        result = u0 + u1;
        u0 = u1;
        u1 = result;
    }
    std::cout << "Nombre de Fibonacci : f(" << debut << ") = " << result << std::endl;
    return 0;
}