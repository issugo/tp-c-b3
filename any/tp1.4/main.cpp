#include <iostream>

int main(int argc, char* argv[]) {
    int number = atoi(argv[1]), recurence = 0, result = number;
    std::cout << "Bienvenue dans la suite de syracuse, vous avez choisi : " << number << std::endl;
    std::cout << "U" << recurence << " = " << number << std::endl;
    while (result != 1) {
        (result % 2) == 0 ? result = result/2 : result = (3 * result) + 1;
        ++recurence;
        std::cout << "U" << recurence << " = " << result << std::endl;
    }
    return 0;
}