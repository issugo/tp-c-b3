#include <iostream>
#include <cmath>
#include <cfloat>

int main(int argc, char* argv[]) {
    float number;
    char* endPtr;
    char* entre = new char[20];
    if (argc == 1) {
        do {
            std::cout << "Entrez le nombre voulu : ", std::cin >> entre;
            number = strtol(entre, &endPtr, 10);
        }while(endPtr == entre);
    } else {
        number = strtol(argv[1], &endPtr, 10);
        if (endPtr == argv[1]) {
            do {
                std::cout << "Entrez le nombre voulu : ", std::cin >> entre;
                number = strtol(entre, &endPtr, 10);
            }while(endPtr == entre);
        }
    }
    if ((errno == ERANGE && (number == FLT_MAX || number == FLT_MIN))
        || (errno != 0 && number == 0)) {
        perror("strtol");
        exit(EXIT_FAILURE);
    }
    std::cout << "Exponentielle de " << number << " = " << exp(number) << std::endl;
    std::cout << "Cosinus de " << number << " = " << cos(number) << std::endl;
    std::cout << "Sinus de " << number << " = " << sin(number) << std::endl;
    delete[] entre;
    return 0;
}
