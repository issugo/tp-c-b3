#include <iostream>
#include <random>
#include <limits>

int main(int argc, char* argv[]) {
    long num, inverse, random, coup, entre;
    char* endPtr;
    char* guess = new char[20];
    char retry = 'N';

    if (argc == 1) {
        do {
            std::cout << "Entrez la limite pour deviner : ", std::cin >> guess;
            num = strtol(guess, &endPtr, 10);
        }while(endPtr == guess);
    } else {
        num = strtol(argv[1], &endPtr, 10);
        if (endPtr == argv[1]) {
            do {
                std::cout << "Entrez la limite pour deviner : ", std::cin >> guess;
                num = strtol(guess, &endPtr, 10);
            }while(endPtr == guess);
        }
    }

    if ((errno == ERANGE && (num == LONG_MAX || num == LONG_MIN))
        || (errno != 0 && num == 0)) {
        perror("strtol");
        exit(EXIT_FAILURE);
    }

    inverse = -1 * num;
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(inverse, std::nextafter(num, std::numeric_limits<long>::max()));
    do {
        random = dis(gen);
        coup = 0;
        std::cout << "Le nombre à trouver est compris entre " << inverse << " et " << num << std::endl;
        do {
            std::cout << "Entrez un nombre :  ", std::cin >> guess;
            entre = strtol(guess, &endPtr, 10);
            std::cout << (entre > random ? "Trop grand" : (entre < random ? "Trop petit" : "")) << std::endl;
            ++coup;
        } while (entre != random);
        std::cout << "Bravo le chiffre est bien " << random << ", vous avez gagné en " << coup << " coup(s)." << std::endl;
        std::cout << "Voulez-vous recommencez [y/o]: ", std::cin >> retry;
    }while (retry == 'o' || retry == 'O' || retry == 'y' || retry == 'Y');
    delete[] guess;
    return 0;
}
