#include <iostream>

int main(int argc, char* argv[]) {
    int number, result = 1, temp = 0;
    char* endPtr;
    char* entre = new char[20];
    if (argc == 1) {
        do {
            std::cout << "Entrez le nombre voulu : ", std::cin >> entre;
            number = strtol(entre, &endPtr, 10);
        }while(endPtr == entre);
    } else {
        number = strtol(argv[1], &endPtr, 10);
        if (endPtr == argv[1]) {
            do {
                std::cout << "Entrez le nombre voulu : ", std::cin >> entre;
                number = strtol(entre, &endPtr, 10);
            }while(endPtr == entre);
        }
    }
    if ((errno == ERANGE && (number == INT_MAX || number == INT_MIN))
        || (errno != 0 && number == 0)) {
        perror("strtol");
        exit(EXIT_FAILURE);
    }
    while (result < number) {
        ++temp;
        result = result * temp;
    }
    std::cout << temp-1 << "! <= " << number << std::endl;
    return 0;
}
