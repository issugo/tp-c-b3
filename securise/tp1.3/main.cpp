#include <iostream>
#include <ctime>

int main(int argc, char* argv[]) {
    srand(time(nullptr));
    char* endPtr;
    char* entre = new char[20];
    int nbAlumettes, enlevage = 0, joueur = 1;
    if (argc == 1) {
        do {
            std::cout << "Entrez le nombre d'allumettes : ", std::cin >> entre;
            nbAlumettes = strtol(entre, &endPtr, 10);
        }while(endPtr == entre);
    } else {
        nbAlumettes = strtol(argv[1], &endPtr, 10);
        if (endPtr == argv[1]) {
            do {
                std::cout << "Entrez le nombre d'allumettes : ", std::cin >> entre;
                nbAlumettes = strtol(entre, &endPtr, 10);
            }while(endPtr == entre);
        }
    }
    if ((errno == ERANGE && (nbAlumettes == LONG_MAX || nbAlumettes == LONG_MIN))
        || (errno != 0 && nbAlumettes == 0)) {
        perror("strtol");
        exit(EXIT_FAILURE);
    }
    joueur = rand() % 2 + 1;
    std::cout << "nombre d'alumettes de départ : " << nbAlumettes << std::endl;
    do {
        joueur == 1 ? joueur = 2 : joueur = 1;
        do {
            std::cout << "joueur " << joueur << " enlève : ", std::cin >> entre;
            enlevage = strtol(entre, &endPtr, 10);
            if (nbAlumettes < enlevage || endPtr == entre) {
                std::cout << "nombre invalide" << std::endl;
            }
        } while(endPtr == entre || enlevage > nbAlumettes);
        nbAlumettes = nbAlumettes - enlevage;
        std::cout << "Il reste " << nbAlumettes << " d'alumettes" << std::endl;
    }while (nbAlumettes > 0);
    std::cout << "joueur " << joueur << " a perdu !!" << std::endl;
    return 0;
}
