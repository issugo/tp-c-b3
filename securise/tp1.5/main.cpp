#include <iostream>
#include <cfloat>

int main(int argc, char* argv[]) {
    float a, b, p, i, result = 0;
    char* endPtr;
    char* entre = new char[20];

    switch (argc) {
        case 1:
            do {
                std::cout << "Entrez la borne inférieure : ", std::cin >> entre;
                a = strtol(entre, &endPtr, 10);
            }while(endPtr == entre);
            do {
                std::cout << "Entrez la borne supérieure : ", std::cin >> entre;
                b = strtol(entre, &endPtr, 10);
            }while(endPtr == entre);
            do {
                std::cout << "Entrez le pas de calcul : ", std::cin >> entre;
                p = strtol(entre, &endPtr, 10);
            }while(endPtr == entre);
            break;
        case 2:
            a = strtol(argv[1], &endPtr, 10);
            if (endPtr == argv[1]) {
                do {
                    std::cout << "Entrez la borne supérieure : ", std::cin >> entre;
                    a = strtol(entre, &endPtr, 10);
                }while(endPtr == entre);
            }
            do {
                std::cout << "Entrez la borne supérieure : ", std::cin >> entre;
                b = strtol(entre, &endPtr, 10);
            }while(endPtr == entre);
            do {
                std::cout << "Entrez le pas de calcul : ", std::cin >> entre;
                p = strtol(entre, &endPtr, 10);
            }while(endPtr == entre);
            break;
        case 3:
            a = strtol(argv[1], &endPtr, 10);
            if (endPtr == argv[1]) {
                do {
                    std::cout << "Entrez la borne inférieure : ", std::cin >> entre;
                    a = strtol(entre, &endPtr, 10);
                }while(endPtr == entre);
            }
            b = strtol(argv[2], &endPtr, 10);
            if (endPtr == argv[2]) {
                do {
                    std::cout << "Entrez la borne supérieure : ", std::cin >> entre;
                    b = strtol(entre, &endPtr, 10);
                }while(endPtr == entre);
            }
            do {
                std::cout << "Entrez le pas de calcul : ", std::cin >> entre;
                p = strtol(entre, &endPtr, 10);
            }while(endPtr == entre);
            break;
        default:
            a = strtol(argv[1], &endPtr, 10);
            if (endPtr == argv[1]) {
                do {
                    std::cout << "Entrez la borne inférieure : ", std::cin >> entre;
                    a = strtol(entre, &endPtr, 10);
                }while(endPtr == entre);
            }
            b = strtol(argv[2], &endPtr, 10);
            if (endPtr == argv[2]) {
                do {
                    std::cout << "Entrez la borne supérieure : ", std::cin >> entre;
                    b = strtol(entre, &endPtr, 10);
                }while(endPtr == entre);
            }
            p = strtol(argv[3], &endPtr, 10);
            if (endPtr == argv[3]) {
                do {
                    std::cout << "Entrez le pas de calcul : ", std::cin >> entre;
                    p = strtol(entre, &endPtr, 10);
                }while(endPtr == entre);
            }
            break;
    }
    if ((errno == ERANGE && (a == FLT_MAX || a == FLT_MIN))
        || (errno != 0 && a == 0)) {
        perror("strtol");
        exit(EXIT_FAILURE);
    }
    if ((errno == ERANGE && (b == FLT_MAX || b == FLT_MIN))
        || (errno != 0 && b == 0)) {
        perror("strtol");
        exit(EXIT_FAILURE);
    }
    if ((errno == ERANGE && (p == FLT_MAX || p == FLT_MIN))
        || (errno != 0 && p == 0)) {
        perror("strtol");
        exit(EXIT_FAILURE);
    }
    i = a;

    while (i < b) {
        std::cout << "[trace] a=" << a << " ; b=" << b << " ; p=" << p << " ; i=" << i <<" ; result=" << result << std::endl;
        result = result + ((i * i) * p);
        i = i + p;
    }
    std::cout << "L'intégrale de " << a << " à " << b  << "avec un pas de " << p << " est " << result << std::endl;
    return 0;
}
