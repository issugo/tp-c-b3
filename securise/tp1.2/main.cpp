#include <iostream>
#include <ctime>

int main() {
    srand(time(nullptr));
    int de1 = 4, de2 = 4, de4 = 4;
    int* jeu = new int[3];
    for (int i = 1; i <= 3; ++i) {
        for (int y=0; y<3; ++y) {
            if (y == de4 || y == de2 || y == de1) {
                continue;
            } else {
                jeu[y] = rand() % 6 + 1;
            }
        }
        for (int n=0; n<3; ++n) {
            if (de4 == 4 && jeu[n] == 4) {
                de4 = n;
            }
            if (de2 == 4 && jeu[n] == 2) {
                de2 = n;
            }
            if (de1 == 4 && jeu[n] == 1) {
                de1 = n;
            }
        }
        std::cout << "Voici votre jet : " << jeu[0] << " " << jeu[1] << " " << jeu[2] << std::endl;
        system("pause");
        if (de4 != 4 && de2 != 4 && de1 != 4) {
            std::cout << "Bravo !! vous avez *gagné" << std::endl;
            break;
        }
    }
    return 0;
}
