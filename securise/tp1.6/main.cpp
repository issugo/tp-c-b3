#include <iostream>

int main(int argc, char* argv[]) {
    int debut, u0 = 0, u1 = 1, result, n;
    char* endPtr;
    char* entre = new char[20];
    if (argc == 1) {
        do {
            std::cout << "Entrez le nombre à calculer : ", std::cin >> entre;
            debut = strtol(entre, &endPtr, 10);
        }while(endPtr == entre);
    } else {
        debut = strtol(argv[1], &endPtr, 10);
        if (endPtr == argv[1]) {
            do {
                std::cout << "Entrez le nombre à calculer : ", std::cin >> entre;
                debut = strtol(entre, &endPtr, 10);
            }while(endPtr == entre);
        }
    }
    if ((errno == ERANGE && (debut == LONG_MAX || debut == LONG_MIN))
        || (errno != 0 && debut == 0)) {
        perror("strtol");
        exit(EXIT_FAILURE);
    }
    n = debut;
    for (int i = 0; i < n-1; ++i) {
        result = u0 + u1;
        u0 = u1;
        u1 = result;
    }
    std::cout << "Nombre de Fibonacci : f(" << debut << ") = " << result << std::endl;
    return 0;
}
