#include <iostream>

int main(int argc, char* argv[]) {
    int number , recurence = 0, result;
    char* endPtr;
    char* entre = new char[20];
    if (argc == 1) {
        do {
            std::cout << "Entrez le nombre voulu : ", std::cin >> entre;
            number = strtol(entre, &endPtr, 10);
        }while(endPtr == entre);
    } else {
        number = strtol(argv[1], &endPtr, 10);
        if (endPtr == argv[1]) {
            do {
                std::cout << "Entrez le nombre voulu : ", std::cin >> entre;
                number = strtol(entre, &endPtr, 10);
            }while(endPtr == entre);
        }
    }
    if ((errno == ERANGE && (number == LONG_MAX || number == LONG_MIN))
        || (errno != 0 && number == 0)) {
        perror("strtol");
        exit(EXIT_FAILURE);
    }
    result = number;
    std::cout << "Bienvenue dans la suite de syracuse, vous avez choisi : " << number << std::endl;
    std::cout << "U" << recurence << " = " << number << std::endl;
    while (result != 1) {
        (result % 2) == 0 ? result = result/2 : result = (3 * result) + 1;
        ++recurence;
        std::cout << "U" << recurence << " = " << result << std::endl;
    }
    return 0;
}
